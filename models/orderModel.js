const mongoose = require('mongoose')
//current date formant
var currentdate = new Date(); 
var datetime =  currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear()


const orderSchema = new mongoose.Schema({
    "quantity": {type: Number},
    "totalAmount":{type: Number},
    "productId":{type: String},
    "productName":{type: String},
    "OrderDate":{type: String, default: datetime},
    "userId": {type: String},
    
},{ timestamps: true})


module.exports = mongoose.model('orderModel',orderSchema)