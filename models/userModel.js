const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    "fullName" :{
        type: String,
        required: [true,`Name is required`]
       
    },
    "email":{
        type: String,
        required: [true,`Email is required`],
        unique: true
    },
    "password": {
        type: String,
        required: [true,`Password is required`]
       
    },
    "isAdmin":{
        type: Boolean,
        default: false
    },
    "Orders": [{ orderId:{
                    type:String,
                    required: [true,`productId is required`]
                },
                status:{
                    type: String,
                    default:"InCart"
                },
                enrolledOn:{
                    type: Date, 
                    default: new Date()
                }
}]
},{ timestamps: true})



module.exports = mongoose.model('userModel',userSchema)