const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, `Product name is required`],
        unique: true
    },
    description: {
        type: String,
        required: [true, `Product description is required`]
    },
    price: {
        type: Number,
        required: [true, `Price is required`]
    },
    isOffered: {
        type: Boolean,
        default: true
    },
    inStock: {
        type: Number
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, `userId is required`]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ],
    pictureName: {type:String}

}, {timestamps: true})

module.exports = mongoose.model("Product", productSchema);