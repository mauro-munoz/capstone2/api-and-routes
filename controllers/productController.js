const myProduct = require('./../models/productModel')


//create a new product
module.exports.createProduct = async (reqBody) => {
    return await myProduct.findOne({productName: reqBody.productName}).then((result)=>{
        if(result != null && result.productName == reqBody.productName) {
            return {message: `Product with name ${result.productName} is already registered`}
        } else {
            let newProduct = new myProduct({
                "productName": reqBody.productName,
                "description": reqBody.description,
                "price": reqBody.price,
                "pictureName": reqBody.pictureName,
            })
            return newProduct.save().then(()=>
            {return {message: `Product ${newProduct.productName} has been registered successfully`}}
            ).catch(err => err.message)
        }
    })
}

//get all products in database
module.exports.getAllProducts = async () => {
    return await myProduct.find({}).then(result=>result).catch(err=> err)
}

//get one product
module.exports.getOneProduct = async (id) => {
    return await myProduct.findById(id).then(result=>result).catch(err => {return err.message})
}

//update product
module.exports.productUpdate = async(id,reqBody) => {
    const product = {
        productName: reqBody.productName,
        description: reqBody.description,
        price: reqBody.price,
        isOffered: reqBody.isOffered,
        inStock: reqBody.inStock,
        pictureName: reqBody.pictureName,
    }
    return await myProduct.findByIdAndUpdate(id,{$set:product},{new:true}).then(result=>result).catch(err=>err)
}

//get all active product
module.exports.activeProducts = async () => {
    return await myProduct.find({isOffered: true}).then(result=>result).catch(err=> err)
}

//archive product
module.exports.archiveProduct = async(id) => {
    return await myProduct.findByIdAndUpdate(id,{$set:{isOffered:false}},{new:true}).then(result=>result).catch(err=> err)
}

//unarchive product
module.exports.unarchiveProduct = async(id) => {
    return await myProduct.findByIdAndUpdate(id,{$set:{isOffered:true}},{new:true}).then(result=>result).catch(err=> err)
}

//delete product
module.exports.deleteProduct = async(id) => {
    return await myProduct.findByIdAndRemove(id).then(result=>result).catch(err=> err)
}