const myUser = require('./../models/userModel')
const myProduct = require('./../models/productModel')
const myOrder = require('./../models/orderModel')
const CryptoJS = require('crypto-js');
const {createToken,decode} = require('./../auth')

//create a new user
module.exports.createUser = async (reqBody) => {
    return await myUser.findOne({email: reqBody.email}).then((result)=>{
        if(result != null && result.email == reqBody.email) {
            return {message:`User with email ${result.email} is already registered`}
        } else {
            let newUser = new myUser({
                "fullName": reqBody.fullName,
                "email": reqBody.email,
                "password": CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
            })
            return newUser.save().then(()=>
           { return {message: `User ${newUser.fullName} was successfully registered`}}
            ).catch(err => {message: err.message} )
        }
    })
}

//get all users in database
module.exports.getAllUsers = async () => {
    return await myUser.find({}).then(result=>result)
}

//login user
module.exports.loginUser = async (reqBody) => {
    return await myUser.findOne({email: reqBody.email}).then(result=>{
        if(result == null){
            return {message:"User does not exist"}
        }else{
            if(result !== null){
              let decrypted =  CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                // console.log(reqBody.password)

                if(reqBody.password == decrypted){
                    // console.log(`mau`)
                    
                   return {message: `${createToken(result)}`}
                } else {
                    return {message: `auth failed`}
                }
            }else {
                return err}
        }
    })
}

//give admin access to user
module.exports.adminAccess = async(id,reqBody) => {
    const userAccess = {
        isAdmin: reqBody.isAdmin
    }
    return await myUser.findByIdAndUpdate(id,{$set:userAccess},{new:true}).then(result=>result).catch(err=>err)
}

//delete user 
module.exports.deleteUser = async(reqBody) => {
    return await myUser.findOneAndDelete({fullName:reqBody.fullName}).then(result=>result).catch(err=>err)
}

//forgot password
module.exports.forgotPassword = async (reqBody) => {
    return await myUser.findOne({email: reqBody.email}).then(result=>{
        let decrypted =  CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
        return decrypted
    }).catch(err=>err)
}

//user profile
module.exports.userProfile = async (id) => {
    return await myUser.findById(id).then(result=>result).catch(err=>{err})
}

