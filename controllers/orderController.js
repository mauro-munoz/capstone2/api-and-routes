const myOrder = require('./../models/orderModel')
const CryptoJS = require('crypto-js');
const myUser = require('./../models/userModel')
const {createToken} = require('./../auth')
const myProduct = require('./../models/productModel')


//add new order
module.exports.addOrder = async (data) => {
    const{userId, productId, quantity,productName} = data
    const productPrice = await myProduct.findById(productId).then(result=>result)
    // console.log(productPrice.price)
    const updatedUser = await myUser.findById(userId).then(result =>{
        // console.log(userId)
        let newOrder = new myOrder({
            quantity: quantity,
            userId: userId,
            productId: productId,
            productName: productName
        })
        // console.log(newOrder.quantity)
        let total = newOrder.quantity * productPrice.price
        // console.log(total)
        newOrder.totalAmount = total
        newOrder.save().then(result=>result?true:false)
        result.Orders.push({orderId:newOrder._id})
        return result.save().then(result=>result ? true : false)
    })
    if(updatedUser){
        return true
    }else { return false}
}

//get all orders in database
module.exports.getAllOrders = async () => {
    return await myOrder.find({}).then(result=>result)
}

//delete order 
module.exports.deleteOrder = async(id) => {
    return await myOrder.findByIdAndDelete(id).then(result=>result).catch(err=>err)
}

//find orders
module.exports.findOrder = async(id) => {
    return await myOrder.findById(id).then(result=>result).catch(err=>err)
}

//retrieve orders for a non admin user
module.exports.userOrders = async(id) => {
    
    return await myOrder.find({userId: id}).then(result=>result).catch(err=>err)
}