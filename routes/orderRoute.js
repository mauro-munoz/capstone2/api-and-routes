const express = require('express');
const router = express.Router();
const controller = require(`./../controllers/orderController`)
const {verifyToken, decode, verifyAdmin} = require(`./../auth`)

//retrieve orders for a non admin user
router.get(`/user-orders`, verifyToken, async (req,res)=>{
    const user = decode(req.headers.authorization).isAdmin
    const id  = decode(req.headers.authorization).id
   
   if(user == false){
    try{
        await controller.userOrders(id).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
   }else{
       res.send(`Only user with access can search`)
   }
})

//add order for a user
router.post(`/add-order`, verifyToken, async (req,res)=>{
    const user = decode(req.headers.authorization).isAdmin
   const data={
       userId: decode(req.headers.authorization).id,
       productId: req.body.productId,
       quantity : req.body.quantity,
       productName: req.body.productName
   }
   if(user == false){
    try{
       await controller.addOrder(data).then(result => res.send(result))
    }catch(err){res.status(500).send(err.message)}
   }else{
       res.send(`Only user with access can purchase order`)
   }
})

//get all orders in database
router.get(`/`, verifyAdmin,async (req,res)=> {
    // res.send(`welcome to orders`)
    try{
        await controller.getAllOrders().then(result => res.send(result))
    }catch(err){res.status(500).send(err.message)}
})


//delete order for a user
router.delete(`/delete-order/:orderId`, async (req, res)=>{
    try{
       await controller.deleteOrder(req.params.orderId).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})

//find order
router.get(`/:orderId`, async (req,res)=>{
    try{
        await controller.findOrder(req.params.orderId).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})



module.exports = router