const express = require('express');
const router = express.Router();
const controller = require(`./../controllers/userController`)
const {verifyToken, decode, verifyAdmin} = require(`./../auth`)

//add a new user
router.post(`/add-user`, async (req, res) => {
    try{
        await controller.createUser(req.body).then(result => res.status(200).send(result))
    }catch(err){res.status(500).send(err)}
})

//get all users in database
router.get(`/`, async (req,res)=> {
    try{
        await controller.getAllUsers().then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})

//login a user
router.post(`/login`, async (req, res) => {
    try{
        await controller.loginUser(req.body).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})

//give admin access
router.put(`/isAdmin/:userId`,verifyAdmin,async (req, res)=>{
    const userId = (decode(req.headers.authorization).id)
    const isAdmin = (decode(req.headers.authorization).isAdmin)
    if(isAdmin == true){
          try{
        await controller.adminAccess(req.params.userId, req.body).then(result=>res.send(result))
    }catch(err){return res.status(500).send(err.message)}
    }else {res.send(`You are not authorized`)}
  
})

//delete user
router.delete(`/delete`, async (req,res)=>{
    try{
        await controller.deleteUser(req.body).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})

//forgot password
router.post(`/forgot-password`,verifyAdmin, async (req,res)=>{
    try{
        await controller.forgotPassword(req.body).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})


//get user profile

router.get(`/profile`,verifyToken,async (req,res)=>{
    // console.log(req.headers.authorization)
    // console.log(`welcome to get request`)
    const userId = decode(req.headers.authorization).id
    // console.log(userId)
    try{
        controller.userProfile(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})



//export router to be imported in index.js
module.exports = router