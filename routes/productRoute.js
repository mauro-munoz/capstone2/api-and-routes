const express = require('express');
const router = express.Router();
const controller = require(`./../controllers/productController`)
const {verifyToken, decode, verifyAdmin} = require(`./../auth`)

//archive product
router.put(`/archive/:productId`, verifyAdmin, async (req, res)=>{
    try{
        await controller.archiveProduct(req.params.productId).then(result => res.send(result))
    }catch(err){return res.status(500).send(err.message)}
})

//unarchive product
router.put(`/unarchive/:productId`, verifyAdmin, async (req, res)=>{
    try{
        await controller.unarchiveProduct(req.params.productId).then(result => res.send(result))
    }catch(err){return res.status(500).send(err.message)}
})

//delete product
router.delete(`/delete/:productId`, verifyAdmin, async (req,res)=>{
    try{
        await controller.deleteProduct(req.params.productId).then(result => res.send(result))
    }catch(err){return res.status(500).send(err.message)}
})

//add a new product
router.post(`/add-product`,verifyAdmin, async (req, res) => {
    const isAdmin = (decode(req.headers.authorization).isAdmin)
    if(isAdmin == true){
        try{
            await controller.createProduct(req.body).then(result => res.send(result))
        }catch(err){res.status(500).send(err.message)}
    }else{res.status(403).send(`You are not authorized`)}
})

//get active products
router.get(`/active-products`, verifyToken, async (req,res)=> {
    try{
        await controller.activeProducts().then(result => res.send(result))
    }catch(err){res.status(500).send(err.message)}
})


//get all products in database
router.get(`/`,verifyToken ,async (req,res)=> {
    try{
        await controller.getAllProducts().then(result => res.send(result))
    }catch(err){res.status(500).send(err.message)}
})

//get a single product
router.get(`/:productId`,verifyToken,async (req, res)=>{
    try{
        await controller.getOneProduct(req.params.productId).then(result => res.send(result))
    }catch(err){res.status(500).send(err)}
})

//update product
router.put(`/product-update/:productId`,verifyAdmin,async (req, res)=>{
    const userId = (decode(req.headers.authorization).id)
    const isAdmin = (decode(req.headers.authorization).isAdmin)
    if(isAdmin == true){
          try{
        await controller.productUpdate(req.params.productId,req.body).then(result=>res.send(result))
    }catch(err){return res.status(500).send(err.message)}
    }else {res.send(`You are not authorized`)}
  
})



//export router to be imported in index.js
module.exports = router