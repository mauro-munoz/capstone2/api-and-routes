const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const PORT = process.env.PORT || 3000;
const app = express();
const cors = require(`cors`)

//Middleware used to parse data from client
app.use(express.json())
app.use(express.urlencoded({extended:true}))
//prevents blocking of request from client esp different domains
app.use(cors())

//Connect to database in MongoDB
mongoose.connect(process.env.MONGO_URL_MAURO, {useNewUrlParser: true, useUnifiedTopology: true})
//Test connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
// db.on(`error`,()=>{console.log(`connection error`)})
db.once('open', () => {console.log(`Connected to Database`)
});

//Import users routes
const usersRouter = require(`./routes/userRoute`)
//Link user routes to main app 

app.use(`/users`,usersRouter)

const productRouter = require (`./routes/productRoute`)
app.use(`/products`,productRouter)

const orderRouter = require (`./routes/orderRoute`)
app.use(`/orders`,orderRouter)

//listen to port in local hose
app.listen(PORT,()=>console.log(`Server is active at port ${PORT}`));